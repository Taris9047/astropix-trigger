# A simple voltage converter for trigger signal board.

A board bridges between Atlas board to Digilent Nexys Video board. Mainly delivering a trigger signal from Atlas board to the Nexys FPGA board that will communicate Astropix boards.

In short, this is a custom PMOD extension board that will interface the Nexys Video FPGA (or any other Digilent FPGA boards) to Atlas board.

# Design concept can be found at...
https://docs.google.com/presentation/d/12Q8QdalHZKYPj27mnB5MjG81byUPvnqYDJiNvLSNxv4/edit#slide=id.p

# Connectors to Atlas
LEMO Connectors(https://www.lemo.com/en)